<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\RbacMemoryManager;
use PhpExtended\Rbac\Role;
use PhpExtended\Rbac\RoleStatus;
use PHPUnit\Framework\TestCase;

/**
 * RoleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\Role
 *
 * @internal
 *
 * @small
 */
class RoleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Role
	 */
	protected Role $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals('id', $this->_object->getIdentifier());
	}
	
	public function testGetRolename() : void
	{
		$this->assertEquals('name', $this->_object->getRolename());
	}
	
	public function testGetRoleStatus() : void
	{
		$this->assertEquals(new RoleStatus('id', true), $this->_object->getRoleStatus());
	}
	
	public function testGetRules() : void
	{
		$this->assertEquals([], $this->_object->getRules());
	}
	
	public function testGetAssignedUsers() : void
	{
		$this->assertEquals([], $this->_object->getAssignedUsers());
	}
	
	public function testGetAssignedGroups() : void
	{
		$this->assertEquals([], $this->_object->getAssignedGroups());
	}
	
	public function testGetParentRoles() : void
	{
		$this->assertEquals([], $this->_object->getParentRoles());
	}
	
	public function testGetParentRolesRecursive() : void
	{
		$this->assertEquals([], $this->_object->getParentRolesRecursive());
	}
	
	public function testGetChildrenRoles() : void
	{
		$this->assertEquals([], $this->_object->getChildrenRoles());
	}
	
	public function testGetChildrenRolesRecursive() : void
	{
		$this->assertEquals([], $this->_object->getChildrenRolesRecursive());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Role(new RbacMemoryManager(), 'id', 'name', new RoleStatus('id', true));
	}
	
}
