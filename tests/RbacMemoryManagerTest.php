<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\Group;
use PhpExtended\Rbac\GroupStatus;
use PhpExtended\Rbac\LoopException;
use PhpExtended\Rbac\RbacMemoryManager;
use PhpExtended\Rbac\Role;
use PhpExtended\Rbac\RoleStatus;
use PhpExtended\Rbac\Rule;
use PhpExtended\Rbac\User;
use PhpExtended\Rbac\UserStatus;
use PHPUnit\Framework\TestCase;

/**
 * RbacMemoryManagerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\RbacMemoryManager
 *
 * @internal
 *
 * @small
 */
class RbacMemoryManagerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RbacMemoryManager
	 */
	protected RbacMemoryManager $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetUser() : void
	{
		$this->assertNull($this->_object->getUser('userid'));
		$this->_object->addUserStatus('userstatus', true);
		$this->_object->addUser('userid', 'username', $this->_object->getUserStatus('userstatus'));
		$this->assertEquals(new User($this->_object, 'userid', 'username', new UserStatus('userstatus', true)), $this->_object->getUser('userid'));
		$this->_object->removeUser('userid');
		$this->assertNull($this->_object->getUser('userid'));
	}
	
	public function testGetGroupsFromUser() : void
	{
		$this->_object->addUserStatus('userstatus', true);
		$this->_object->addUser('userid', 'username', $this->_object->getUserStatus('userstatus'));
		$user = $this->_object->getUser('userid');
		$this->assertEquals([], $this->_object->getGroupsFromUser($user));
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('groupid', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group = $this->_object->getGroup('groupid');
		$this->assertEquals([], $this->_object->getUsersFromGroup($group));
		$this->_object->addUserToGroup($user, $group);
		$this->assertEquals([$group], $this->_object->getGroupsFromUser($user));
		$this->assertEquals([$user], $this->_object->getUsersFromGroup($group));
		$this->_object->removeUserFromGroup($user, $group);
		$this->assertEquals([], $this->_object->getGroupsFromUser($user));
		$this->assertEquals([], $this->_object->getUsersFromGroup($group));
	}
	
	public function testGetGroupsRecursiveFromUser() : void
	{
		$this->_object->addUserStatus('userstatus', true);
		$this->_object->addUser('userid', 'username', $this->_object->getUserStatus('userstatus'));
		$user = $this->_object->getUser('userid');
		$this->assertEquals([], $this->_object->getRecursiveGroupsFromUser($user));
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('groupid', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group = $this->_object->getGroup('groupid');
		$this->_object->addUserToGroup($user, $group);
		$this->assertEquals([$group], $this->_object->getRecursiveGroupsFromUser($user));
		$this->_object->addGroup('groupid2', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group2 = $this->_object->getGroup('groupid2');
		$this->_object->addChildGroup($group, $group2);
		$this->assertEquals([$group, $group2], $this->_object->getRecursiveGroupsFromUser($user));
	}
	
	public function testGetRolesFromUser() : void
	{
		$this->_object->addUserStatus('userstatus', true);
		$this->_object->addUser('userid', 'username', $this->_object->getUserStatus('userstatus'));
		$user = $this->_object->getUser('userid');
		$this->assertEquals([], $this->_object->getRolesFromUser($user));
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('roleid', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role = $this->_object->getRole('roleid');
		$this->assertEquals([], $this->_object->getUsersFromRole($role));
		$this->_object->addRoleToUser($user, $role);
		$this->assertEquals([$role], $this->_object->getRolesFromUser($user));
		$this->assertEquals([$user], $this->_object->getUsersFromRole($role));
		$this->_object->removeRoleFromUser($user, $role);
		$this->assertEquals([], $this->_object->getRolesFromUser($user));
		$this->assertEquals([], $this->_object->getUsersFromRole($role));
	}
	
	public function testGetRolesRecursiveFromUser() : void
	{
		$this->_object->addUserStatus('userstatus', true);
		$this->_object->addUser('userid', 'username', $this->_object->getUserStatus('userstatus'));
		$user = $this->_object->getUser('userid');
		$this->assertEquals([], $this->_object->getRecursiveRolesFromUser($user));
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('roleid', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role = $this->_object->getRole('roleid');
		$this->_object->addRoleToUser($user, $role);
		$this->assertEquals([$role], $this->_object->getRecursiveRolesFromUser($user));
		$this->_object->addRole('roleid2', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role2 = $this->_object->getRole('roleid2');
		$this->_object->addChildRole($role, $role2);
		$this->assertEquals([$role, $role2], $this->_object->getRecursiveRolesFromUser($user));
	}
	
	public function testGetRolesFromGroup() : void
	{
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('groupid', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group = $this->_object->getGroup('groupid');
		$this->assertEquals([], $this->_object->getRolesFromGroup($group));
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('roleid', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role = $this->_object->getRole('roleid');
		$this->assertEquals([], $this->_object->getGroupsFromRole($role));
		$this->_object->addRoleToGroup($group, $role);
		$this->assertEquals([$role], $this->_object->getRolesFromGroup($group));
		$this->assertEquals([$group], $this->_object->getGroupsFromRole($role));
		$this->_object->removeRoleFromGroup($group, $role);
		$this->assertEquals([], $this->_object->getRolesFromGroup($group));
		$this->assertEquals([], $this->_object->getGroupsFromRole($role));
	}
	
	public function testGetRolesFromGroupFromUser() : void
	{
		$this->_object->addUserStatus('userstatus', true);
		$this->_object->addUser('userid', 'username', $this->_object->getUserStatus('userstatus'));
		$user = $this->_object->getUser('userid');
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('groupid', 'username', $this->_object->getGroupStatus('groupstatus'));
		$group = $this->_object->getGroup('groupid');
		$this->_object->addUserToGroup($user, $group);
		$this->assertEquals([], $this->_object->getRolesFromGroupsFromUser($user));
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('roleid', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role = $this->_object->getRole('roleid');
		$this->_object->addRoleToGroup($group, $role);
		$this->assertEquals([$role], $this->_object->getRolesFromGroupsFromUser($user));
		$this->_object->addGroup('groupid2', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group2 = $this->_object->getGroup('groupid2');
		$this->_object->addChildGroup($group, $group2);
		$this->_object->addRole('roleid2', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role2 = $this->_object->getRole('roleid2');
		$this->_object->addRoleToGroup($group2, $role2);
		$this->assertEquals([$role, $role2], $this->_object->getRolesFromGroupsFromUser($user));
	}
	
	public function testGetAllRolesFromUser() : void
	{
		$this->_object->addUserStatus('userstatus', true);
		$this->_object->addUser('userid', 'username', $this->_object->getUserStatus('userstatus'));
		$user = $this->_object->getUser('userid');
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('roleid', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role = $this->_object->getRole('roleid');
		$this->_object->addRoleToUser($user, $role);
		$this->assertEquals([$role], $this->_object->getAllRolesFromUser($user));
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('groupid', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group = $this->_object->getGroup('groupid');
		$this->_object->addUserToGroup($user, $group);
		$this->_object->addRole('roleid2', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role2 = $this->_object->getRole('roleid2');
		$this->_object->addRoleToGroup($group, $role2);
		$this->assertEquals([$role, $role2], $this->_object->getAllRolesFromUser($user));
		$this->_object->addGroup('groupid2', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group2 = $this->_object->getGroup('groupid2');
		$this->_object->addChildGroup($group, $group2);
		$this->_object->addRole('roleid3', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role3 = $this->_object->getRole('roleid3');
		$this->_object->addRoleToGroup($group2, $role3);
		$this->assertEquals([$role, $role2, $role3], $this->_object->getAllRolesFromUser($user));
	}
	
	public function testGetGroup() : void
	{
		$this->assertNull($this->_object->getGroup('groupid'));
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('groupid', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$this->assertEquals(new Group($this->_object, 'groupid', 'groupname', new GroupStatus('groupstatus', true)), $this->_object->getGroup('groupid'));
		$this->_object->removeGroup('groupid');
		$this->assertNull($this->_object->getGroup('groupid'));
	}
	
	public function testGroupHierarchy() : void
	{
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('group1', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group1 = $this->_object->getGroup('group1');
		$this->assertEquals([], $this->_object->getParentGroups($group1));
		$this->assertEquals([], $this->_object->getParentGroupsRecursive($group1));
		$this->assertEquals([], $this->_object->getChildrenGroups($group1));
		$this->assertEquals([], $this->_object->getChildrenGroupsRecursive($group1));
		$this->_object->addGroup('group2', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group2 = $this->_object->getGroup('group2');
		$this->_object->addGroup('group3', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group3 = $this->_object->getGroup('group3');
		$this->_object->addChildGroup($group1, $group2);
		$this->_object->addChildGroup($group2, $group3);
		$this->assertEquals([$group2], $this->_object->getChildrenGroups($group1));
		$this->assertEquals([$group2, $group3], $this->_object->getChildrenGroupsRecursive($group1));
		$this->assertEquals([$group2], $this->_object->getParentGroups($group3));
		$this->assertEquals([$group2, $group1], $this->_object->getParentGroupsRecursive($group3));
		$this->_object->removeChildGroup($group1, $group2);
		$this->_object->removeChildGroup($group2, $group3);
		$this->assertEquals([], $this->_object->getChildrenGroups($group2));
		$this->assertEquals([], $this->_object->getParentGroups($group2));
	}
	
	public function testGroupHierarchyTree() : void
	{
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('group1', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group1 = $this->_object->getGroup('group1');
		$this->_object->addGroup('group2', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group2 = $this->_object->getGroup('group2');
		$this->_object->addGroup('group3', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group3 = $this->_object->getGroup('group3');
		$this->_object->addGroup('group4', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group4 = $this->_object->getGroup('group4');
		$this->_object->addChildGroup($group1, $group2);
		$this->_object->addChildGroup($group2, $group3);
		$this->_object->addChildGroup($group4, $group1);
		$this->assertEquals([$group1, $group4], $this->_object->getParentGroupsRecursive($group2));
	}
	
	public function testGroupHierarchyLoop() : void
	{
		$this->expectException(LoopException::class);
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('group1', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group1 = $this->_object->getGroup('group1');
		$this->_object->addChildGroup($group1, $group1);
	}
	
	public function testGroupHierarchyLongLoop() : void
	{
		$this->expectException(LoopException::class);
		$this->_object->addGroupStatus('groupstatus', true);
		$this->_object->addGroup('group1', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group1 = $this->_object->getGroup('group1');
		$this->_object->addGroup('group2', 'groupname', $this->_object->getGroupStatus('groupstatus'));
		$group2 = $this->_object->getGroup('group2');
		$this->_object->addChildGroup($group1, $group2);
		$this->_object->addChildGroup($group2, $group1);
	}
	
	public function testGetRole() : void
	{
		$this->assertNull($this->_object->getRole('roleid'));
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('roleid', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$this->assertEquals(new Role($this->_object, 'roleid', 'rolename', new RoleStatus('rolestatus', true)), $this->_object->getRole('roleid'));
		$this->_object->removeRole('roleid');
		$this->assertNull($this->_object->getRole('roleid'));
	}
	
	public function testRoleHierarchy() : void
	{
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('role1', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role1 = $this->_object->getRole('role1');
		$this->assertEquals([], $this->_object->getParentRoles($role1));
		$this->assertEquals([], $this->_object->getParentRolesRecursive($role1));
		$this->assertEquals([], $this->_object->getChildrenRoles($role1));
		$this->assertEquals([], $this->_object->getChildrenRolesRecursive($role1));
		$this->_object->addRole('role2', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role2 = $this->_object->getRole('role2');
		$this->_object->addRole('role3', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role3 = $this->_object->getRole('role3');
		$this->_object->addChildRole($role1, $role2);
		$this->_object->addChildRole($role2, $role3);
		$this->assertEquals([$role2], $this->_object->getChildrenRoles($role1));
		$this->assertEquals([$role2, $role3], $this->_object->getChildrenRolesRecursive($role1));
		$this->assertEquals([$role2], $this->_object->getParentRoles($role3));
		$this->assertEquals([$role2, $role1], $this->_object->getParentRolesRecursive($role3));
		$this->_object->removeChildRole($role1, $role2);
		$this->_object->removeChildRole($role2, $role3);
		$this->assertEquals([], $this->_object->getChildrenRoles($role2));
		$this->assertEquals([], $this->_object->getParentRoles($role2));
	}
	
	public function testRoleHierarchyTree() : void
	{
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('role1', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role1 = $this->_object->getRole('role1');
		$this->_object->addRole('role2', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role2 = $this->_object->getRole('role2');
		$this->_object->addRole('role3', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role3 = $this->_object->getRole('role3');
		$this->_object->addRole('role4', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role4 = $this->_object->getRole('role4');
		$this->_object->addChildRole($role1, $role2);
		$this->_object->addChildRole($role2, $role3);
		$this->_object->addChildRole($role4, $role1);
		$this->assertEquals([$role1, $role4], $this->_object->getParentRolesRecursive($role2));
	}
	
	public function testRoleHierarchyLoop() : void
	{
		$this->expectException(LoopException::class);
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('role1', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role1 = $this->_object->getRole('role1');
		$this->_object->addChildRole($role1, $role1);
	}
	
	public function testRoleHierarchyLongLoop() : void
	{
		$this->expectException(LoopException::class);
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('role1', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role1 = $this->_object->getRole('role1');
		$this->_object->addRole('role2', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role2 = $this->_object->getRole('role2');
		$this->_object->addChildRole($role1, $role2);
		$this->_object->addChildRole($role2, $role1);
	}
	
	public function testGetRulesFromRole() : void
	{
		$this->_object->addRoleStatus('rolestatus', true);
		$this->_object->addRole('roleid', 'rolename', $this->_object->getRoleStatus('rolestatus'));
		$role = $this->_object->getRole('roleid');
		$this->assertEquals([], $this->_object->getRulesFromRole($role));
		$this->_object->addRule('ruleid', 'rulename', ['param' => 'value']);
		$rule = $this->_object->getRule('ruleid');
		$this->_object->addRuleToRole($role, $rule);
		$this->assertEquals([new Rule('ruleid', 'rulename', ['param' => 'value'])], $this->_object->getRulesFromRole($role));
		$this->_object->removeRuleFromRole($role, $rule);
		$this->assertEquals([], $this->_object->getRulesFromRole($role));
	}
	
	public function testGetRule() : void
	{
		$this->assertNull($this->_object->getRule('ruleid'));
		$this->_object->addRule('ruleid', 'rulename', ['param' => 'value']);
		$this->assertEquals(new Rule('ruleid', 'rulename', ['param' => 'value']), $this->_object->getRule('ruleid'));
		$this->_object->removeRule('ruleid');
		$this->assertNull($this->_object->getRule('ruleid'));
	}
	
	public function testGetGroupStatus() : void
	{
		$this->assertEquals(new GroupStatus('default', false), $this->_object->getGroupStatus('groupstatus'));
		$this->_object->addGroupStatus('groupstatus', true);
		$this->assertEquals(new GroupStatus('groupstatus', true), $this->_object->getGroupStatus('groupstatus'));
		$this->_object->removeGroupStatus('groupstatus');
		$this->assertEquals(new GroupStatus('default', false), $this->_object->getGroupStatus('groupstatus'));
	}
	
	public function testGetRoleStatus() : void
	{
		$this->assertEquals(new RoleStatus('default', false), $this->_object->getRoleStatus('rolestatus'));
		$this->_object->addRoleStatus('rolestatus', true);
		$this->assertEquals(new RoleStatus('rolestatus', true), $this->_object->getRoleStatus('rolestatus'));
		$this->_object->removeRoleStatus('rolestatus');
		$this->assertEquals(new RoleStatus('default', false), $this->_object->getRoleStatus('rolestatus'));
	}
	
	public function testGetUserStatus() : void
	{
		$this->assertEquals(new UserStatus('default', false), $this->_object->getUserStatus('userstatus'));
		$this->_object->addUserStatus('userstatus', true);
		$this->assertEquals(new UserStatus('userstatus', true), $this->_object->getUserStatus('userstatus'));
		$this->_object->removeUserStatus('userstatus');
		$this->assertEquals(new UserStatus('default', false), $this->_object->getUserStatus('userstatus'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new RbacMemoryManager();
	}
	
}
