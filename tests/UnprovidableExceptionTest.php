<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\UnprovidableException;
use PHPUnit\Framework\TestCase;

/**
 * UnprovidableExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\UnprovidableException
 *
 * @internal
 *
 * @small
 */
class UnprovidableExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UnprovidableException
	 */
	protected UnprovidableException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UnprovidableException();
	}
	
}
