<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\RbacMemoryManager;
use PhpExtended\Rbac\User;
use PhpExtended\Rbac\UserStatus;
use PHPUnit\Framework\TestCase;

/**
 * UserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\User
 *
 * @internal
 *
 * @small
 */
class UserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var User
	 */
	protected User $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals('id', $this->_object->getIdentifier());
	}
	
	public function testGetUsername() : void
	{
		$this->assertEquals('name', $this->_object->getUsername());
	}
	
	public function testGetUserStatus() : void
	{
		$this->assertEquals(new UserStatus('id', true), $this->_object->getUserStatus());
	}
	
	public function testGetAssignedGroups() : void
	{
		$this->assertEquals([], $this->_object->getAssignedGroups());
	}
	
	public function testGetAssignedRoles() : void
	{
		$this->assertEquals([], $this->_object->getAssignedRoles());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new User(new RbacMemoryManager(), 'id', 'name', new UserStatus('id', true));
	}
	
}
