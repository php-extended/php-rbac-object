<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\Group;
use PhpExtended\Rbac\GroupStatus;
use PhpExtended\Rbac\RbacMemoryManager;
use PHPUnit\Framework\TestCase;

/**
 * GroupTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\Group
 *
 * @internal
 *
 * @small
 */
class GroupTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Group
	 */
	protected Group $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals('id', $this->_object->getIdentifier());
	}
	
	public function testGetGroupname() : void
	{
		$this->assertEquals('name', $this->_object->getGroupname());
	}
	
	public function testGetGroupStatus() : void
	{
		$this->assertEquals(new GroupStatus('id', true), $this->_object->getGroupStatus());
	}
	
	public function testGetAssignedUsers() : void
	{
		$this->assertEquals([], $this->_object->getAssignedUsers());
	}
	
	public function testGetAssignedRoles() : void
	{
		$this->assertEquals([], $this->_object->getAssignedRoles());
	}
	
	public function testGetParentGroups() : void
	{
		$this->assertEquals([], $this->_object->getParentGroups());
	}
	
	public function testGetParentGroupsRecursive() : void
	{
		$this->assertEquals([], $this->_object->getParentGroupsRecursive());
	}
	
	public function testGetChildrenGroups() : void
	{
		$this->assertEquals([], $this->_object->getChildrenGroups());
	}
	
	public function testGetChildrenGroupsRecursive() : void
	{
		$this->assertEquals([], $this->_object->getChildrenGroupsRecursive());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Group(new RbacMemoryManager(), 'id', 'name', new GroupStatus('id', true));
	}
	
}
