<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\UserStatus;
use PHPUnit\Framework\TestCase;

/**
 * UserStatusTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\UserStatus
 *
 * @internal
 *
 * @small
 */
class UserStatusTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UserStatus
	 */
	protected UserStatus $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals('id', $this->_object->getIdentifier());
	}
	
	public function testIsActive() : void
	{
		$this->assertTrue($this->_object->isActive());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UserStatus('id', true);
	}
	
}
