<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\GroupInterface;
use PhpExtended\Rbac\ProviderInterface;
use PhpExtended\Rbac\Role;
use PhpExtended\Rbac\RoleInterface;
use PhpExtended\Rbac\RoleStatus;
use PhpExtended\Rbac\RuleInterface;
use PhpExtended\Rbac\UnprovidableException;
use PhpExtended\Rbac\UserInterface;
use PHPUnit\Framework\TestCase;

/**
 * RoleExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\Role
 * 
 * @internal
 * 
 * @small
 */
class RoleExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var Role
	 */
	protected Role $_object;
	
	public function testGetRules() : void
	{
		$this->assertEquals([], $this->_object->getRules());
	}
	
	public function testGetAssignedUsers() : void
	{
		$this->assertEquals([], $this->_object->getAssignedUsers());
	}
	
	public function testGetAssignedGroups() : void
	{
		$this->assertEquals([], $this->_object->getAssignedGroups());
	}
	
	public function testGetParentRoles() : void
	{
		$this->assertEquals([], $this->_object->getParentRoles());
	}
	
	public function testGetParentRolesRecursive() : void
	{
		$this->assertEquals([], $this->_object->getParentRolesRecursive());
	}
	
	public function testGetChildrenRoles() : void
	{
		$this->assertEquals([], $this->_object->getChildrenRoles());
	}
	
	public function testGetChildrenRolesRecursive() : void
	{
		$this->assertEquals([], $this->_object->getChildrenRolesRecursive());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Role(new class() implements ProviderInterface
		{
			
			public function __toString() : string
			{
				return __CLASS__;
			}
			
			public function getUser(string $userId) : ?UserInterface
			{
				throw new UnprovidableException();
			}
			
			public function getGroupsFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRecursiveGroupsFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRolesFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRecursiveRolesFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRolesFromGroupsFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getAllRolesFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getGroup(string $groupId) : ?GroupInterface
			{
				throw new UnprovidableException();
			}
			
			public function getParentGroups(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getParentGroupsRecursive(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenGroups(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenGroupsRecursive(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getUsersFromGroup(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRolesFromGroup(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRole(string $roleId) : ?RoleInterface
			{
				throw new UnprovidableException();
			}
			
			public function getParentRoles(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getParentRolesRecursive(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenRoles(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenRolesRecursive(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getGroupsFromRole(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getUsersFromRole(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRulesFromRole(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRule(string $ruleId) : ?RuleInterface
			{
				throw new UnprovidableException();
			}
			
		}, 'id', 'name', new RoleStatus('id', true));
	}
	
}
