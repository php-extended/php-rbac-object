<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\LoopException;
use PHPUnit\Framework\TestCase;

/**
 * LoopExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\LoopException
 *
 * @internal
 *
 * @small
 */
class LoopExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LoopException
	 */
	protected LoopException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LoopException();
	}
	
}
