<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\RbacMemoryManager;
use PhpExtended\Rbac\SimpleAccessor;
use PHPUnit\Framework\TestCase;

/**
 * SimpleAccessorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\SimpleAccessor
 *
 * @internal
 *
 * @small
 */
class SimpleAccessorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var SimpleAccessor
	 */
	protected SimpleAccessor $_object;
	
	/**
	 * The memory to store all the data.
	 * 
	 * @var RbacMemoryManager
	 */
	protected $_memory;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testNoUserNoAccess() : void
	{
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserStatusInactive() : void
	{
		$this->_memory->addUserStatus('userstatus', false);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserInactiveNoAccess() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserRoleActive() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'username', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToUser($user, $role);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserRoleNonActive() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', false);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToUser($user, $role);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserRoleWithNoArgsRuleCheckNoArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'username', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToUser($user, $role);
		$this->_memory->addRule('ruleid', 'rulename', []);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserRoleWithNoArgsRuleCheckArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'username', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToUser($user, $role);
		$this->_memory->addRule('ruleid', 'rulename', []);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid', ['param' => 'value']));
	}
	
	public function testUserRoleWithArgsRuleCheckNoArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'username', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToUser($user, $role);
		$this->_memory->addRule('ruleid', 'rulename', ['param' => 'value']);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserRoleWithArgsRuleCheckArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'username', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToUser($user, $role);
		$this->_memory->addRule('ruleid', 'rulename', ['param' => 'value']);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid', ['param' => 'value']));
	}
	
	public function testUserRoleCascade() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleadmin', 'roleadmin', $this->_memory->getRoleStatus('rolestatus'));
		$radmin = $this->_memory->getRole('roleadmin');
		$this->_memory->addRoleToUser($user, $radmin);
		$this->_memory->addRole('rolemodo', 'rolemodo', $this->_memory->getRoleStatus('rolestatus'));
		$rmodo = $this->_memory->getRole('rolemodo');
		$this->_memory->addChildRole($radmin, $rmodo);
		$this->_memory->addRole('roleuser', 'roleuser', $this->_memory->getRoleStatus('rolestatus'));
		$ruser = $this->_memory->getRole('roleuser');
		$this->_memory->addChildRole($rmodo, $ruser);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleuser'));
	}
	
	public function testUserRoleCascadeFailsUserInactive() : void
	{
		$this->_memory->addUserStatus('userstatus', false);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleadmin', 'roleadmin', $this->_memory->getRoleStatus('rolestatus'));
		$radmin = $this->_memory->getRole('roleadmin');
		$this->_memory->addRoleToUser($user, $radmin);
		$this->_memory->addRole('rolemodo', 'rolemodo', $this->_memory->getRoleStatus('rolestatus'));
		$rmodo = $this->_memory->getRole('rolemodo');
		$this->_memory->addChildRole($radmin, $rmodo);
		$this->_memory->addRole('roleuser', 'roleuser', $this->_memory->getRoleStatus('rolestatus'));
		$ruser = $this->_memory->getRole('roleuser');
		$this->_memory->addChildRole($rmodo, $ruser);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleuser'));
	}
	
	public function testUserRoleCascadeFailsRoleInactive() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', false);
		$this->_memory->addRole('roleadmin', 'roleadmin', $this->_memory->getRoleStatus('rolestatus'));
		$radmin = $this->_memory->getRole('roleadmin');
		$this->_memory->addRoleToUser($user, $radmin);
		$this->_memory->addRole('rolemodo', 'rolemodo', $this->_memory->getRoleStatus('rolestatus'));
		$rmodo = $this->_memory->getRole('rolemodo');
		$this->_memory->addChildRole($radmin, $rmodo);
		$this->_memory->addRole('roleuser', 'roleuser', $this->_memory->getRoleStatus('rolestatus'));
		$ruser = $this->_memory->getRole('roleuser');
		$this->_memory->addChildRole($rmodo, $ruser);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleuser'));
	}
	
	public function testUserRoleCascadeFailsWrongRoleOrder() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleadmin', 'roleadmin', $this->_memory->getRoleStatus('rolestatus'));
		$radmin = $this->_memory->getRole('roleadmin');
		$this->_memory->addRoleToUser($user, $radmin);
		$this->_memory->addRole('rolemodo', 'rolemodo', $this->_memory->getRoleStatus('rolestatus'));
		$rmodo = $this->_memory->getRole('rolemodo');
		$this->_memory->addChildRole($rmodo, $radmin);
		$this->_memory->addRole('roleuser', 'roleuser', $this->_memory->getRoleStatus('rolestatus'));
		$ruser = $this->_memory->getRole('roleuser');
		$this->_memory->addChildRole($ruser, $rmodo);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleuser'));
	}
	
	public function testUserGroupDirectAccess() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupDirectFailedAccess() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupDirectAccessFailedUserDisabled() : void
	{
		$this->_memory->addUserStatus('userstatus', false);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupDirectAccessFailedGroupDisabled() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', false);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupDirectAccessFailedRoleDisabled() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', false);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupDirectAccessWithNoArgsRuleCheckNoArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->_memory->addRule('ruleid', 'rulename', []);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupDirectAccessWithNoArgsRuleCheckArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->_memory->addRule('ruleid', 'rulename', []);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid', ['param' => 'value']));
	}
	
	public function testUserGroupDirectAccessWithArgsRuleCheckNoArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->_memory->addRule('ruleid', 'rulename', ['param' => 'value']);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertFalse($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupDirectAccessWithArgsRuleCheckArgs() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($group, $role);
		$this->_memory->addRule('ruleid', 'rulename', ['param' => 'value']);
		$rule = $this->_memory->getRule('ruleid');
		$this->_memory->addRuleToRole($role, $rule);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid', ['param' => 'value']));
	}
	
	public function testUserCascadeGroupAccess() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupadmin', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$gadm = $this->_memory->getGroup('groupadmin');
		$this->_memory->addUserToGroup($user, $gadm);
		$this->_memory->addGroup('groupmodo', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$gmod = $this->_memory->getGroup('groupmodo');
		$this->_memory->addChildGroup($gadm, $gmod);
		$this->_memory->addGroup('groupuser', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$gusr = $this->_memory->getGroup('groupuser');
		$this->_memory->addChildGroup($gmod, $gusr);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleid', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$role = $this->_memory->getRole('roleid');
		$this->_memory->addRoleToGroup($gusr, $role);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleid'));
	}
	
	public function testUserGroupCascadeRoleAccess() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupadmin', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$gadm = $this->_memory->getGroup('groupadmin');
		$this->_memory->addUserToGroup($user, $gadm);
		$this->_memory->addGroup('groupmodo', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$gmod = $this->_memory->getGroup('groupmodo');
		$this->_memory->addChildGroup($gadm, $gmod);
		$this->_memory->addGroup('groupuser', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$gusr = $this->_memory->getGroup('groupuser');
		$this->_memory->addChildGroup($gmod, $gusr);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleadmin', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$radm = $this->_memory->getRole('roleadmin');
		$this->_memory->addRoleToGroup($gusr, $radm);
		$this->_memory->addRole('rolemodo', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$rmod = $this->_memory->getRole('rolemodo');
		$this->_memory->addChildRole($radm, $rmod);
		$this->_memory->addRole('roleuser', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$rusr = $this->_memory->getRole('roleuser');
		$this->_memory->addChildRole($rmod, $rusr);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleuser'));
	}
	
	public function testUserCascadeGroupCascadeRoleAccess() : void
	{
		$this->_memory->addUserStatus('userstatus', true);
		$this->_memory->addUser('userid', 'username', $this->_memory->getUserStatus('userstatus'));
		$user = $this->_memory->getUser('userid');
		$this->_memory->addGroupStatus('groupstatus', true);
		$this->_memory->addGroup('groupid', 'groupname', $this->_memory->getGroupStatus('groupstatus'));
		$group = $this->_memory->getGroup('groupid');
		$this->_memory->addUserToGroup($user, $group);
		$this->_memory->addRoleStatus('rolestatus', true);
		$this->_memory->addRole('roleadmin', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$radm = $this->_memory->getRole('roleadmin');
		$this->_memory->addRoleToGroup($group, $radm);
		$this->_memory->addRole('rolemodo', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$rmod = $this->_memory->getRole('rolemodo');
		$this->_memory->addChildRole($radm, $rmod);
		$this->_memory->addRole('roleuser', 'rolename', $this->_memory->getRoleStatus('rolestatus'));
		$rusr = $this->_memory->getRole('roleuser');
		$this->_memory->addChildRole($rmod, $rusr);
		$this->assertTrue($this->_object->checkAccess('userid', 'roleuser'));
	}
	
	public function testClearCacheForUser() : void
	{
		$this->assertTrue($this->_object->clearCacheForUser('userid'));
	}
	
	public function testClearCacheForRole() : void
	{
		$this->assertTrue($this->_object->clearCacheForRole('roleid'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new SimpleAccessor($this->_memory = new RbacMemoryManager());
	}
	
}
