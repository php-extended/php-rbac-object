<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\UnregistrableException;
use PHPUnit\Framework\TestCase;

/**
 * UnregistrableExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\UnregistrableException
 *
 * @internal
 *
 * @small
 */
class UnregistrableExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UnregistrableException
	 */
	protected UnregistrableException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UnregistrableException();
	}
	
}
