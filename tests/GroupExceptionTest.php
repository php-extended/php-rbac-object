<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\Group;
use PhpExtended\Rbac\GroupInterface;
use PhpExtended\Rbac\GroupStatus;
use PhpExtended\Rbac\ProviderInterface;
use PhpExtended\Rbac\RoleInterface;
use PhpExtended\Rbac\RuleInterface;
use PhpExtended\Rbac\UnprovidableException;
use PhpExtended\Rbac\UserInterface;
use PHPUnit\Framework\TestCase;

/**
 * GroupExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\Group
 * 
 * @internal
 * 
 * @small
 */
class GroupExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var Group
	 */
	protected Group $_object;
	
	public function testGetAssignedUsers() : void
	{
		$this->assertEquals([], $this->_object->getAssignedUsers());
	}
	
	public function testGetAssignedRoles() : void
	{
		$this->assertEquals([], $this->_object->getAssignedRoles());
	}
	
	public function testGetParentGroups() : void
	{
		$this->assertEquals([], $this->_object->getParentGroups());
	}
	
	public function testGetParentGroupsRecursive() : void
	{
		$this->assertEquals([], $this->_object->getParentGroupsRecursive());
	}
	
	public function testGetChildrenGroups() : void
	{
		$this->assertEquals([], $this->_object->getChildrenGroups());
	}
	
	public function testGetChildrenGroupsRecursive() : void
	{
		$this->assertEquals([], $this->_object->getChildrenGroupsRecursive());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Group(new class() implements ProviderInterface
		{
			
			public function __toString() : string
			{
				return __CLASS__;
			}
			
			public function getUser(string $userId) : ?UserInterface
			{
				throw new UnprovidableException();
			}
			
			public function getGroupsFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRecursiveGroupsFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRolesFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRecursiveRolesFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRolesFromGroupsFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getAllRolesFromUser(UserInterface $user) : array
			{
				throw new UnprovidableException();
			}
			
			public function getGroup(string $groupId) : ?GroupInterface
			{
				throw new UnprovidableException();
			}
			
			public function getParentGroups(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getParentGroupsRecursive(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenGroups(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenGroupsRecursive(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getUsersFromGroup(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRolesFromGroup(GroupInterface $group) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRole(string $roleId) : ?RoleInterface
			{
				throw new UnprovidableException();
			}
			
			public function getParentRoles(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getParentRolesRecursive(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenRoles(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getChildrenRolesRecursive(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getGroupsFromRole(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getUsersFromRole(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRulesFromRole(RoleInterface $role) : array
			{
				throw new UnprovidableException();
			}
			
			public function getRule(string $ruleId) : ?RuleInterface
			{
				throw new UnprovidableException();
			}
			
		}, 'id', 'name', new GroupStatus('id', true));
	}
	
}
