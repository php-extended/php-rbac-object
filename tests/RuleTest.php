<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Rbac\RbacMemoryManager;
use PhpExtended\Rbac\Role;
use PhpExtended\Rbac\RoleStatus;
use PhpExtended\Rbac\Rule;
use PhpExtended\Rbac\User;
use PhpExtended\Rbac\UserStatus;
use PHPUnit\Framework\TestCase;

/**
 * RuleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Rbac\Rule
 *
 * @internal
 *
 * @small
 */
class RuleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Rule
	 */
	protected Rule $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@[id]', $this->_object->__toString());
	}
	
	public function testGetIdentifier() : void
	{
		$this->assertEquals('id', $this->_object->getIdentifier());
	}
	
	public function testGetRulename() : void
	{
		$this->assertEquals('name', $this->_object->getRulename());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals(['user' => 'allowedUser', 'role' => 'allowedRole', 'key' => 'value'], $this->_object->getData());
	}
	
	public function testValidateWrongUser() : void
	{
		$this->assertFalse($this->_object->validate(
			new User(new RbacMemoryManager(), 'wrong', 'wrong', new UserStatus('id', true)),
			new Role(new RbacMemoryManager(), 'wrong', 'wrong', new RoleStatus('id', true)),
		));
	}
	
	public function testValidateWrongRole() : void
	{
		$this->assertFalse($this->_object->validate(
			new User(new RbacMemoryManager(), 'allowedUser', 'right', new UserStatus('id', true)),
			new Role(new RbacMemoryManager(), 'wrong', 'wrong', new RoleStatus('id', true)),
		));
	}
	
	public function testValidateWrongDataKey() : void
	{
		$this->assertFalse($this->_object->validate(
			new User(new RbacMemoryManager(), 'allowedUser', 'right', new UserStatus('id', true)),
			new Role(new RbacMemoryManager(), 'allowedRole', 'right', new RoleStatus('id', true)),
			['wrong' => 'value'],
		));
	}
	
	public function testValidateWrongDataValue() : void
	{
		$this->assertFalse($this->_object->validate(
			new User(new RbacMemoryManager(), 'allowedUser', 'right', new UserStatus('id', true)),
			new Role(new RbacMemoryManager(), 'allowedRole', 'right', new RoleStatus('id', true)),
			['key' => 'wrong'],
		));
	}
	
	public function testValidateAllRules() : void
	{
		$this->assertTrue($this->_object->validate(
			new User(new RbacMemoryManager(), 'allowedUser', 'right', new UserStatus('id', true)),
			new Role(new RbacMemoryManager(), 'allowedRole', 'right', new RoleStatus('id', true)),
			['key' => 'value'],
		));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Rule('id', 'name', ['user' => 'allowedUser', 'role' => 'allowedRole', 'key' => 'value']);
	}
	
}
