# php-extended/php-rbac-object

A library that implements the php-rbac-interface libraries.


![coverage](https://gitlab.com/php-extended/php-rbac-object/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-rbac-object/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-rbac-object ^8`


## Basic Usage

This library may be used the following way :

First, you should provide an implementation of `PhpExtended\Rbac\ProviderInterface`. The class
`PhpExtended\Rbac\RbacMemoryManager` is an in-memory example of what
is needed. You might want to develop your own implementation based on the
storage system that you use.


```php

use PhpExtended\Rbac\RbacMemoryManager;

$memory = new RbacMemoryManager();

// STEP 1 : adds the user

$memory->addUserStatus('userstatus', true); // create an active status
$ustatus = $memory->getUserStatus('userstatus'); // get the object
$memory->addUser('userid', 'username', $ustatus); // create an user
$user = $memory->getUser('userid'); // get the object

// STEP 2 : adds the user into a group

$memory->addGroupStatus('groupstatus', true); // create an active status
$gstatus = $memory->getGroupStatus('groupstatus'); // get the object
$memory->addGroup('groupid', 'groupname', $gstatus); // create a group
$group = $memory->getGroup('groupid'); // get the object
$memory->addUserToGroup($user, $group); // put the user to the group

// STEP 3 : adds the role to the group

$memory->addRoleStatus('rolestatus', true); // create an active status
$rstatus = $memory->getRoleStatus('rolestatus'); // get the object
$memory->addRole('roleid', 'rolename', $rstatus); // create a role
$role = $memory->getRole('roleid'); // get the object
$memory->addRoleToGroup($group, $role); // gives the role to the group

// STEP 4 : check whether the user has the role

$accessor = new SimpleAccessor($memory);

$accessor->checkAccess($user, $role); // returns true

```

This library also supports nesting a hierarhcy of groups (like admins, moderators and regular users) and nested roles (like module access, controller access and method access).

It also supports custom rules to be checked when a role is applied for it to
grant access when asked for.

This library is inspired by the yii2 rbac library, but with the possibility to
add users into groups to grant accesses en-masse.


## TODO

- Add psr3 loggable provider
- Add psr6 cached provider
- Add psr16 cached provider
- Add psr7 compatible cached provider
- Add default library for statuses


## License

MIT (See [license file](LICENSE)).
