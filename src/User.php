<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

/**
 * User class file.
 * 
 * This is a simple implementation of the UserInterface.
 * 
 * @author Anastaszor
 */
class User implements UserInterface
{
	
	/**
	 * The provider for the dependancies.
	 *
	 * @var ProviderInterface
	 */
	protected ProviderInterface $_provider;
	
	/**
	 * The identifier of the user.
	 *
	 * @var string
	 */
	protected string $_identifier;
	
	/**
	 * The name of the user.
	 *
	 * @var string
	 */
	protected string $_username;
	
	/**
	 * The status of the user.
	 *
	 * @var UserStatusInterface
	 */
	protected UserStatusInterface $_userStatus;
	
	/**
	 * Buils a new User with its dependancies.
	 * 
	 * @param ProviderInterface $provider
	 * @param string $identifier
	 * @param string $username
	 * @param UserStatusInterface $userStatus
	 */
	public function __construct(
		ProviderInterface $provider,
		string $identifier,
		string $username,
		UserStatusInterface $userStatus
	) {
		$this->_provider = $provider;
		$this->_identifier = $identifier;
		$this->_username = $username;
		$this->_userStatus = $userStatus;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_identifier.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getUsername()
	 */
	public function getUsername() : string
	{
		return $this->_username;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getUserStatus()
	 */
	public function getUserStatus() : UserStatusInterface
	{
		return $this->_userStatus;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getAssignedGroups()
	 */
	public function getAssignedGroups() : array
	{
		try
		{
			return $this->_provider->getGroupsFromUser($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a user object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserInterface::getAssignedRoles()
	 */
	public function getAssignedRoles() : array
	{
		try
		{
			return $this->_provider->getRolesFromUser($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a user object
			return [];
		}
	}
	
}
