<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

/**
 * SimpleAccessor class file.
 * 
 * This is a simple implementation of the AccessorInterface.
 * 
 * @author Anastaszor
 */
class SimpleAccessor implements AccessorInterface
{
	
	/**
	 * The provider from which data is gathered.
	 * 
	 * @var ProviderInterface
	 */
	protected ProviderInterface $_provider;
	
	/**
	 * Builds a new SimpleAccessor with no cache based on the given provider.
	 * 
	 * @param ProviderInterface $provider
	 */
	public function __construct(ProviderInterface $provider)
	{
		$this->_provider = $provider;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\AccessorInterface::checkAccess()
	 */
	public function checkAccess(string $userId, string $roleId, array $params = []) : bool
	{
		$user = $this->_provider->getUser($userId);
		if(null === $user)
		{
			return false;
		}
		
		if(!$user->getUserStatus()->isActive())
		{
			return false;
		}
		
		$role = $this->_provider->getRole($roleId);
		if(null === $role)
		{
			return false;
		}
		
		$asUser = $this->checkUserAccessRecursive($user, $role, $params);
		if($asUser)
		{
			return true;
		}
		
		foreach($user->getAssignedGroups() as $group)
		{
			$asGroup = $this->checkGroupAccessRecursive($user, $group, $role, $params);
			if($asGroup)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets whether the user is allowed to the given role assuming the params.
	 * 
	 * @param UserInterface $user
	 * @param RoleInterface $role
	 * @param array<string, string> $params
	 * @return boolean
	 */
	public function checkUserAccessRecursive(UserInterface $user, RoleInterface $role, array $params = []) : bool
	{
		if(!$role->getRoleStatus()->isActive())
		{
			return false;
		}
		
		$rules = $role->getRules();
		$ruleAreOk = 0 === \count($rules);
		
		/** @var RuleInterface $rule */
		foreach($rules as $rule)
		{
			if($rule->validate($user, $role, $params))
			{
				$ruleAreOk = true;
			}
		}
		
		if(!$ruleAreOk)
		{
			return false;
		}
		
		/** @var RoleInterface $assignedRole */
		foreach($user->getAssignedRoles() as $assignedRole)
		{
			if($assignedRole->getIdentifier() === $role->getIdentifier())
			{
				return true;
			}
		}
		
		foreach($role->getParentRoles() as $parentRole)
		{
			if($this->checkUserAccessRecursive($user, $parentRole, $params))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets whether the user is allowed to the given role assuming a path
	 * through the groups and the params.
	 * 
	 * @param UserInterface $user
	 * @param GroupInterface $group
	 * @param RoleInterface $role
	 * @param array<string, string> $params
	 * @return boolean
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function checkGroupAccessRecursive(UserInterface $user, GroupInterface $group, RoleInterface $role, array $params = []) : bool
	{
		if(!$group->getGroupStatus()->isActive())
		{
			return false;
		}
		
		if(!$role->getRoleStatus()->isActive())
		{
			return false;
		}
		
		$rules = $role->getRules();
		$ruleAreOk = 0 === \count($rules);
		
		/** @var RuleInterface $rule */
		foreach($rules as $rule)
		{
			if($rule->validate($user, $role, $params))
			{
				$ruleAreOk = true;
			}
		}
		
		if(!$ruleAreOk)
		{
			return false;
		}
		
		/** @var RoleInterface $assignedRole */
		foreach($group->getAssignedRoles() as $assignedRole)
		{
			if($assignedRole->getIdentifier() === $role->getIdentifier())
			{
				return true;
			}
		}
		
		foreach($role->getParentRoles() as $parentRole)
		{
			if($this->checkGroupAccessRecursive($user, $group, $parentRole))
			{
				return true;
			}
		}
		
		foreach($group->getChildrenGroups() as $childGroup)
		{
			if($this->checkGroupAccessRecursive($user, $childGroup, $role))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\AccessorInterface::clearCacheForUser()
	 */
	public function clearCacheForUser(string $userId) : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\AccessorInterface::clearCacheForRole()
	 */
	public function clearCacheForRole(string $roleId) : bool
	{
		return true;
	}
	
}
