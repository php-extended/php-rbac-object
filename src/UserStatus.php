<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

/**
 * UserStatus class file.
 * 
 * This is a simple implementation of the UserStatusInterface.
 * 
 * @author Anastaszor
 */
class UserStatus implements UserStatusInterface
{
	
	/**
	 * The identifier of this status.
	 *
	 * @var string
	 */
	protected string $_identifier;
	
	/**
	 * Whether this user is active.
	 *
	 * @var boolean
	 */
	protected bool $_isActive;
	
	/**
	 * Builds a new UserStatus with its dependancies.
	 *
	 * @param string $identifier
	 * @param boolean $isActive
	 */
	public function __construct(string $identifier, bool $isActive)
	{
		$this->_identifier = $identifier;
		$this->_isActive = $isActive;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_identifier.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserStatusInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\UserStatusInterface::isActive()
	 */
	public function isActive() : bool
	{
		return $this->_isActive;
	}
	
}
