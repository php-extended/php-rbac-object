<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

/**
 * Group class file.
 * 
 * This is a simple implementation of the GroupInterface.
 * 
 * @author Anastaszor
 */
class Group implements GroupInterface
{
	
	/**
	 * The provider for the dependancies.
	 * 
	 * @var ProviderInterface
	 */
	protected ProviderInterface $_provider;
	
	/**
	 * The identifier of the group.
	 * 
	 * @var string
	 */
	protected string $_identifier;
	
	/**
	 * The name of the group.
	 * 
	 * @var string
	 */
	protected string $_groupname;
	
	/**
	 * The status of the group.
	 * 
	 * @var GroupStatusInterface
	 */
	protected GroupStatusInterface $_groupStatus;
	
	/**
	 * Builds a new Group with its dependancies.
	 * 
	 * @param ProviderInterface $provider
	 * @param string $identifier
	 * @param string $groupname
	 * @param GroupStatusInterface $groupStatus
	 */
	public function __construct(
		ProviderInterface $provider,
		string $identifier,
		string $groupname,
		GroupStatusInterface $groupStatus
	) {
		$this->_provider = $provider;
		$this->_identifier = $identifier;
		$this->_groupname = $groupname;
		$this->_groupStatus = $groupStatus;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_identifier.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getGroupname()
	 */
	public function getGroupname() : string
	{
		return $this->_groupname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getGroupStatus()
	 */
	public function getGroupStatus() : GroupStatusInterface
	{
		return $this->_groupStatus;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getAssignedUsers()
	 */
	public function getAssignedUsers() : array
	{
		try
		{
			return $this->_provider->getUsersFromGroup($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a group object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getAssignedRoles()
	 */
	public function getAssignedRoles() : array
	{
		try
		{
			return $this->_provider->getRolesFromGroup($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a group object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getParentGroups()
	 */
	public function getParentGroups() : array
	{
		try
		{
			return $this->_provider->getParentGroups($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a group object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getParentGroupsRecursive()
	 */
	public function getParentGroupsRecursive() : array
	{
		try
		{
			return $this->_provider->getParentGroupsRecursive($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a group object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getChildrenGroups()
	 */
	public function getChildrenGroups() : array
	{
		try
		{
			return $this->_provider->getChildrenGroups($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a group object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\GroupInterface::getChildrenGroupsRecursive()
	 */
	public function getChildrenGroupsRecursive() : array
	{
		try
		{
			return $this->_provider->getChildrenGroupsRecursive($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a group object
			return [];
		}
	}
	
}
