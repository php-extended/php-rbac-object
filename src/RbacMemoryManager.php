<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

/**
 * MemoryProvider class file.
 * 
 * This class represents a provider that stores all of its data into memory.
 * This is not a persistant memory and should not be used in production.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.TooManyMethods")
 */
class RbacMemoryManager implements ProviderInterface
{
	
	/**
	 * List of users by id.
	 * 
	 * @var array<string, array{'id': string, 'name': string, 'status': string}>
	 */
	protected array $_users = [];
	
	/**
	 * List of relations between users and groups (user_id => [group_id => 1]).
	 * 
	 * @var array<string, array<string, 1>>
	 */
	protected array $_userGroups = [];
	
	/**
	 * List of relations between users and roles (user_id => [role_id => 1]).
	 * 
	 * @var array<string, array<string, 1>>
	 */
	protected array $_userRoles = [];
	
	/**
	 * List of groups by id.
	 * 
	 * @var array<string, array{'id': string, 'name': string, 'status': string}>
	 */
	protected array $_groups = [];
	
	/**
	 * List of relations between groups (parent_id => [children_id => 1]).
	 * 
	 * @var array<string, array<string, 1>>
	 */
	protected array $_groupHierarchy = [];
	
	/**
	 * List of relations between groups and roles (group_id => [role_id => 1]).
	 * 
	 * @var array<string, array<string, 1>>
	 */
	protected array $_groupRoles = [];
	
	/**
	 * List of roles by id.
	 * 
	 * @var array<string, array{'id': string, 'name': string, 'status': string}>
	 */
	protected array $_roles = [];
	
	/**
	 * List of relations between roles (parent_id => [children_id => 1]).
	 * 
	 * @var array<string, array<string, 1>>
	 */
	protected array $_roleHierarchy = [];
	
	/**
	 * List of relations between roles and rules (role_id => [rule_id => 1]).
	 * 
	 * @var array<string, array<string, 1>>
	 */
	protected array $_roleRules = [];
	
	/**
	 * List of rules by id.
	 * 
	 * @var array<string, array{'id': string, 'name': string, 'data': array<string, string>}>
	 */
	protected array $_rules = [];
	
	/**
	 * List of group statuses by id.
	 * 
	 * @var array<string, array{'id': string, 'active': bool}>
	 */
	protected array $_groupStatuses = [];
	
	/**
	 * List of role statuses by id.
	 * 
	 * @var array<string, array{'id': string, 'active': bool}>
	 */
	protected array $_roleStatuses = [];
	
	/**
	 * List of user statuses by id.
	 * 
	 * @var array<string, array{'id': string, 'active': bool}>
	 */
	protected array $_userStatuses = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds or replace the given user.
	 * 
	 * @param string $userId
	 * @param string $userName
	 * @param UserStatusInterface $status
	 * @return RbacMemoryManager
	 */
	public function addUser(string $userId, string $userName, UserStatusInterface $status) : RbacMemoryManager
	{
		$this->_users[$userId] = ['id' => $userId, 'name' => $userName, 'status' => $status->getIdentifier()];
		
		return $this;
	}
	
	/**
	 * Removes the given user.
	 * 
	 * @param string $userId
	 * @return RbacMemoryManager
	 */
	public function removeUser(string $userId) : RbacMemoryManager
	{
		unset($this->_users[$userId]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getUser()
	 */
	public function getUser(string $userId) : ?UserInterface
	{
		if(!isset($this->_users[$userId]))
		{
			return null;
		}
		
		$data = $this->_users[$userId];
		
		return new User($this, $data['id'], $data['name'], $this->getUserStatus($data['status']));
	}
	
	/**
	 * Adds the given user to the given group.
	 * 
	 * @param UserInterface $user
	 * @param GroupInterface $group
	 * @return RbacMemoryManager
	 */
	public function addUserToGroup(UserInterface $user, GroupInterface $group) : RbacMemoryManager
	{
		$this->_userGroups[$user->getIdentifier()][$group->getIdentifier()] = 1;
		
		return $this;
	}
	
	/**
	 * Removes the given user from the given group.
	 * 
	 * @param UserInterface $user
	 * @param GroupInterface $group
	 * @return RbacMemoryManager
	 */
	public function removeUserFromGroup(UserInterface $user, GroupInterface $group) : RbacMemoryManager
	{
		unset($this->_userGroups[$user->getIdentifier()][$group->getIdentifier()]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getGroupsFromUser()
	 */
	public function getGroupsFromUser(UserInterface $user) : array
	{
		if(!isset($this->_userGroups[$user->getIdentifier()]))
		{
			return [];
		}
		
		$groups = [];
		
		foreach(\array_keys($this->_userGroups[$user->getIdentifier()]) as $groupId)
		{
			$group = $this->getGroup($groupId);
			if(null !== $group)
			{
				$groups[$group->getIdentifier()] = $group;
			}
		}
		
		return \array_values($groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRecursiveGroupsFromUser()
	 */
	public function getRecursiveGroupsFromUser(UserInterface $user) : array
	{
		$groups = [];
		
		foreach($this->getGroupsFromUser($user) as $group)
		{
			$groups[$group->getIdentifier()] = $group;
			
			foreach($this->getChildrenGroupsRecursive($group) as $newGroup)
			{
				$groups[$newGroup->getIdentifier()] = $newGroup;
			}
		}
		
		return \array_values($groups);
	}
	
	/**
	 * Adds the given role to the given user.
	 * 
	 * @param UserInterface $user
	 * @param RoleInterface $role
	 * @return RbacMemoryManager
	 */
	public function addRoleToUser(UserInterface $user, RoleInterface $role) : RbacMemoryManager
	{
		$this->_userRoles[$user->getIdentifier()][$role->getIdentifier()] = 1;
		
		return $this;
	}
	
	/**
	 * Removes the role from the user.
	 * 
	 * @param UserInterface $user
	 * @param RoleInterface $role
	 * @return RbacMemoryManager
	 */
	public function removeRoleFromUser(UserInterface $user, RoleInterface $role) : RbacMemoryManager
	{
		unset($this->_userRoles[$user->getIdentifier()][$role->getIdentifier()]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRolesFromUser()
	 */
	public function getRolesFromUser(UserInterface $user) : array
	{
		if(!isset($this->_userRoles[$user->getIdentifier()]))
		{
			return [];
		}
		
		$roles = [];
		
		foreach(\array_keys($this->_userRoles[$user->getIdentifier()]) as $roleId)
		{
			$role = $this->getRole($roleId);
			if(null !== $role)
			{
				$roles[$role->getIdentifier()] = $role;
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRecursiveRolesFromUser()
	 */
	public function getRecursiveRolesFromUser(UserInterface $user) : array
	{
		$roles = [];
		
		foreach($this->getRolesFromUser($user) as $role)
		{
			$roles[$role->getIdentifier()] = $role;
			
			foreach($this->getChildrenRolesRecursive($role) as $child)
			{
				$roles[$child->getIdentifier()] = $child;
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRolesFromGroupsFromUser()
	 */
	public function getRolesFromGroupsFromUser(UserInterface $user) : array
	{
		$roles = [];
		
		foreach($this->getRecursiveGroupsFromUser($user) as $group)
		{
			foreach($this->getRolesFromGroup($group) as $role)
			{
				$roles[$role->getIdentifier()] = $role;
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getAllRolesFromUser()
	 */
	public function getAllRolesFromUser(UserInterface $user) : array
	{
		$roles = [];
		
		foreach($this->getRolesFromUser($user) as $role)
		{
			$roles[$role->getIdentifier()] = $role;
		}
		
		foreach($this->getRolesFromGroupsFromUser($user) as $role)
		{
			$roles[$role->getIdentifier()] = $role;
		}
		
		return \array_values($roles);
	}
	
	/**
	 * Adds a new group.
	 * 
	 * @param string $groupId
	 * @param string $groupName
	 * @param GroupStatusInterface $status
	 * @return RbacMemoryManager
	 */
	public function addGroup(string $groupId, string $groupName, GroupStatusInterface $status) : RbacMemoryManager
	{
		$this->_groups[$groupId] = ['id' => $groupId, 'name' => $groupName, 'status' => $status->getIdentifier()];
		
		return $this;
	}
	
	/**
	 * Removes the given group.
	 * 
	 * @param string $groupId
	 * @return RbacMemoryManager
	 */
	public function removeGroup(string $groupId) : RbacMemoryManager
	{
		unset($this->_groups[$groupId]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getGroup()
	 */
	public function getGroup(string $groupId) : ?GroupInterface
	{
		if(!isset($this->_groups[$groupId]))
		{
			return null;
		}
		
		$data = $this->_groups[$groupId];
		
		return new Group($this, $data['id'], $data['name'], $this->getGroupStatus($data['status']));
	}
	
	/**
	 * Adds the given child group to the given parent group.
	 * 
	 * @param GroupInterface $parent
	 * @param GroupInterface $child
	 * @return RbacMemoryManager
	 * @throws LoopThrowable if a loop is detected
	 */
	public function addChildGroup(GroupInterface $parent, GroupInterface $child) : RbacMemoryManager
	{
		if($parent->getIdentifier() === $child->getIdentifier())
		{
			$message = 'Cannot add child role to itRbacMemoryManager "{cid}"';
			$context = ['{cid}' => $parent->getIdentifier()];
			
			throw new LoopException(\strtr($message, $context));
		}
		
		/** @var array<integer, string> $stack */
		$stack = \array_keys($this->_groupHierarchy[$child->getIdentifier()] ?? []);
		
		while(!empty($stack))
		{
			$nextid = \array_shift($stack);
			
			if($parent->getIdentifier() === $nextid)
			{
				$message = 'Cannot add child group "{cid}" from parent "{pid}" : loop detected';
				$context = ['{cid}' => $child->getIdentifier(), '{pid}' => $parent->getIdentifier()];
				
				throw new LoopException(\strtr($message, $context));
			}
			
			if(isset($this->_groupHierarchy[$nextid]))
			{
				$stack = \array_merge($stack, \array_keys($this->_groupHierarchy[$nextid]));
			}
		}
		
		$this->_groupHierarchy[$parent->getIdentifier()][$child->getIdentifier()] = 1;
		
		return $this;
	}
	
	/**
	 * Removes the given child group from the given parent.
	 * 
	 * @param GroupInterface $parent
	 * @param GroupInterface $child
	 * @return RbacMemoryManager
	 */
	public function removeChildGroup(GroupInterface $parent, GroupInterface $child) : RbacMemoryManager
	{
		unset($this->_groupHierarchy[$parent->getIdentifier()][$child->getIdentifier()]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentGroups()
	 */
	public function getParentGroups(GroupInterface $group) : array
	{
		$groups = [];
		
		foreach($this->_groupHierarchy as $parentId => $groupList)
		{
			foreach(\array_keys($groupList) as $childId)
			{
				if($group->getIdentifier() === $childId)
				{
					$parent = $this->getGroup($parentId);
					if(null !== $parent)
					{
						$groups[$parent->getIdentifier()] = $parent;
					}
				}
			}
		}
		
		return \array_values($groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentGroupsRecursive()
	 */
	public function getParentGroupsRecursive(GroupInterface $group) : array
	{
		$groups = [];
		
		foreach($this->_groupHierarchy as $parentId => $groupList)
		{
			foreach(\array_keys($groupList) as $childId)
			{
				if($group->getIdentifier() === $childId)
				{
					$parent = $this->getGroup($parentId);
					if(null !== $parent)
					{
						$groups[$parent->getIdentifier()] = $parent;
						
						foreach($this->getParentGroupsRecursive($parent) as $newGroup)
						{
							$groups[$newGroup->getIdentifier()] = $newGroup;
						}
					}
				}
			}
		}
		
		return \array_values($groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenGroups()
	 */
	public function getChildrenGroups(GroupInterface $group) : array
	{
		if(!isset($this->_groupHierarchy[$group->getIdentifier()]))
		{
			return [];
		}
		
		$groups = [];
		
		foreach(\array_keys($this->_groupHierarchy[$group->getIdentifier()]) as $groupId)
		{
			$newGroup = $this->getGroup($groupId);
			if(null !== $newGroup)
			{
				$groups[$newGroup->getIdentifier()] = $newGroup;
			}
		}
		
		return \array_values($groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenGroupsRecursive()
	 */
	public function getChildrenGroupsRecursive(GroupInterface $group) : array
	{
		if(!isset($this->_groupHierarchy[$group->getIdentifier()]))
		{
			return [];
		}
		
		$groups = [];
		
		foreach(\array_keys($this->_groupHierarchy[$group->getIdentifier()]) as $groupId)
		{
			$newGroup = $this->getGroup($groupId);
			if(null !== $newGroup)
			{
				$groups[$newGroup->getIdentifier()] = $newGroup;
				
				foreach($this->getChildrenGroupsRecursive($newGroup) as $newGroup)
				{
					$groups[$newGroup->getIdentifier()] = $newGroup;
				}
			}
		}
		
		return \array_values($groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getUsersFromGroup()
	 */
	public function getUsersFromGroup(GroupInterface $group) : array
	{
		$users = [];
		
		foreach($this->_userGroups as $userId => $groupList)
		{
			foreach(\array_keys($groupList) as $groupId)
			{
				if($group->getIdentifier() === $groupId)
				{
					$user = $this->getUser($userId);
					if(null !== $user)
					{
						$users[$user->getIdentifier()] = $user;
					}
				}
			}
		}
		
		return \array_values($users);
	}
	
	/**
	 * Adds the given role to the given group.
	 * 
	 * @param GroupInterface $group
	 * @param RoleInterface $role
	 * @return RbacMemoryManager
	 */
	public function addRoleToGroup(GroupInterface $group, RoleInterface $role) : RbacMemoryManager
	{
		$this->_groupRoles[$group->getIdentifier()][$role->getIdentifier()] = 1;
		
		return $this;
	}
	
	/**
	 * Removes the given role from the given group.
	 * 
	 * @param GroupInterface $group
	 * @param RoleInterface $role
	 * @return RbacMemoryManager
	 */
	public function removeRoleFromGroup(GroupInterface $group, RoleInterface $role) : RbacMemoryManager
	{
		unset($this->_groupRoles[$group->getIdentifier()][$role->getIdentifier()]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRolesFromGroup()
	 */
	public function getRolesFromGroup(GroupInterface $group) : array
	{
		if(!isset($this->_groupRoles[$group->getIdentifier()]))
		{
			return [];
		}
		
		$roles = [];
		
		foreach($this->_groupRoles as $roleList)
		{
			foreach(\array_keys($roleList) as $roleId)
			{
				$newRole = $this->getRole($roleId);
				if(null !== $newRole)
				{
					$roles[$newRole->getIdentifier()] = $newRole;
				}
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * Adds the given role.
	 * 
	 * @param string $roleId
	 * @param string $roleName
	 * @param RoleStatusInterface $status
	 * @return RbacMemoryManager
	 */
	public function addRole(string $roleId, string $roleName, RoleStatusInterface $status) : RbacMemoryManager
	{
		$this->_roles[$roleId] = ['id' => $roleId, 'name' => $roleName, 'status' => $status->getIdentifier()];
		
		return $this;
	}
	
	/**
	 * Removes the given role.
	 * 
	 * @param string $roleId
	 * @return RbacMemoryManager
	 */
	public function removeRole(string $roleId) : RbacMemoryManager
	{
		unset($this->_roles[$roleId]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRole()
	 */
	public function getRole(string $roleId) : ?RoleInterface
	{
		if(!isset($this->_roles[$roleId]))
		{
			return null;
		}
		
		$data = $this->_roles[$roleId];
		
		return new Role($this, $data['id'], $data['name'], $this->getRoleStatus($data['status']));
	}
	
	/**
	 * Adds the given child role to the parent role.
	 * 
	 * @param RoleInterface $parent
	 * @param RoleInterface $child
	 * @return RbacMemoryManager
	 * @throws LoopThrowable if a loop is detected
	 */
	public function addChildRole(RoleInterface $parent, RoleInterface $child) : RbacMemoryManager
	{
		if($parent->getIdentifier() === $child->getIdentifier())
		{
			$message = 'Cannot add child role to itRbacMemoryManager "{cid}"';
			$context = ['{cid}' => $parent->getIdentifier()];
			
			throw new LoopException(\strtr($message, $context));
		}
		
		/** @var array<integer, string> $stack */
		$stack = \array_keys($this->_roleHierarchy[$child->getIdentifier()] ?? []);
		
		while(!empty($stack))
		{
			$nextid = \array_shift($stack);
			
			if($parent->getIdentifier() === $nextid)
			{
				$message = 'Cannot add child role "{cid}" from parent "{pid}" : loop detected';
				$context = ['{cid}' => $child->getIdentifier(), '{pid}' => $parent->getIdentifier()];
				
				throw new LoopException(\strtr($message, $context));
			}
			
			if(isset($this->_roleHierarchy[$nextid]))
			{
				$stack = \array_merge($stack, \array_keys($this->_roleHierarchy[$nextid]));
			}
		}
		
		$this->_roleHierarchy[$parent->getIdentifier()][$child->getIdentifier()] = 1;
		
		return $this;
	}
	
	/**
	 * Removes the given child role from its parent role.
	 * 
	 * @param RoleInterface $parent
	 * @param RoleInterface $child
	 * @return RbacMemoryManager
	 */
	public function removeChildRole(RoleInterface $parent, RoleInterface $child) : RbacMemoryManager
	{
		unset($this->_roleHierarchy[$parent->getIdentifier()][$child->getIdentifier()]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentRoles()
	 */
	public function getParentRoles(RoleInterface $role) : array
	{
		$roles = [];
		
		foreach($this->_roleHierarchy as $parentId => $roleList)
		{
			foreach(\array_keys($roleList) as $childId)
			{
				if($role->getIdentifier() === $childId)
				{
					$parent = $this->getRole($parentId);
					if(null !== $parent)
					{
						$roles[$parent->getIdentifier()] = $parent;
					}
				}
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getParentRolesRecursive()
	 */
	public function getParentRolesRecursive(RoleInterface $role) : array
	{
		$roles = [];
		
		foreach($this->_roleHierarchy as $parentId => $roleList)
		{
			foreach(\array_keys($roleList) as $childId)
			{
				if($role->getIdentifier() === $childId)
				{
					$parent = $this->getRole($parentId);
					if(null !== $parent)
					{
						$roles[$parent->getIdentifier()] = $parent;
						
						foreach($this->getParentRolesRecursive($parent) as $newRole)
						{
							$roles[$newRole->getIdentifier()] = $newRole;
						}
					}
				}
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenRoles()
	 */
	public function getChildrenRoles(RoleInterface $role) : array
	{
		if(!isset($this->_roleHierarchy[$role->getIdentifier()]))
		{
			return [];
		}
		
		$roles = [];
		
		foreach(\array_keys($this->_roleHierarchy[$role->getIdentifier()]) as $roleId)
		{
			$newRole = $this->getRole($roleId);
			if(null !== $newRole)
			{
				$roles[$roleId] = $newRole;
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getChildrenRolesRecursive()
	 */
	public function getChildrenRolesRecursive(RoleInterface $role) : array
	{
		if(!isset($this->_roleHierarchy[$role->getIdentifier()]))
		{
			return [];
		}
		
		$roles = [];
		
		foreach(\array_keys($this->_roleHierarchy[$role->getIdentifier()]) as $roleId)
		{
			$newRole = $this->getRole($roleId);
			if(null !== $newRole)
			{
				$roles[$roleId] = $newRole;
				
				foreach($this->getChildrenRolesRecursive($newRole) as $newRole)
				{
					$roles[$newRole->getIdentifier()] = $newRole;
				}
			}
		}
		
		return \array_values($roles);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getGroupsFromRole()
	 */
	public function getGroupsFromRole(RoleInterface $role) : array
	{
		$groups = [];
		
		foreach($this->_groupRoles as $groupId => $roleList)
		{
			foreach(\array_keys($roleList) as $newRole)
			{
				if($role->getIdentifier() === $newRole)
				{
					$group = $this->getGroup($groupId);
					if(null !== $group)
					{
						$groups[$groupId] = $group;
					}
				}
			}
		}
		
		return \array_values($groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getUsersFromRole()
	 */
	public function getUsersFromRole(RoleInterface $role) : array
	{
		$users = [];
		
		foreach($this->_userRoles as $userId => $roleList)
		{
			foreach(\array_keys($roleList) as $newRole)
			{
				if($role->getIdentifier() === $newRole)
				{
					$user = $this->getUser($userId);
					if(null !== $user)
					{
						$users[$userId] = $user;
					}
				}
			}
		}
		
		return \array_values($users);
	}
	
	/**
	 * Adds the given rule to the role.
	 * 
	 * @param RoleInterface $role
	 * @param RuleInterface $rule
	 * @return RbacMemoryManager
	 */
	public function addRuleToRole(RoleInterface $role, RuleInterface $rule) : RbacMemoryManager
	{
		$this->_roleRules[$role->getIdentifier()][$rule->getIdentifier()] = 1;
		
		return $this;
	}
	
	/**
	 * Removes the association between role and rule.
	 * 
	 * @param RoleInterface $role
	 * @param RuleInterface $rule
	 * @return RbacMemoryManager
	 */
	public function removeRuleFromRole(RoleInterface $role, RuleInterface $rule) : RbacMemoryManager
	{
		unset($this->_roleRules[$role->getIdentifier()][$rule->getIdentifier()]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRulesFromRole()
	 */
	public function getRulesFromRole(RoleInterface $role) : array
	{
		if(!isset($this->_roleRules[$role->getIdentifier()]))
		{
			return [];
		}
		
		$rulesIds = \array_keys($this->_roleRules[$role->getIdentifier()]);
		$rules = [];
		
		foreach($rulesIds as $ruleId)
		{
			$rule = $this->getRule($ruleId);
			if(null !== $rule)
			{
				$rules[$rule->getIdentifier()] = $rule;
			}
		}
		
		return \array_values($rules);
	}
	
	/**
	 * Adds or replace the given rule.
	 * 
	 * @param string $ruleId
	 * @param string $ruleName
	 * @param array<string, string> $ruleParams
	 * @return RbacMemoryManager
	 */
	public function addRule(string $ruleId, string $ruleName, array $ruleParams) : RbacMemoryManager
	{
		$this->_rules[$ruleId] = ['id' => $ruleId, 'name' => $ruleName, 'data' => $ruleParams];
		
		return $this;
	}
	
	/**
	 * Removes the given rule.
	 * 
	 * @param string $ruleId
	 * @return RbacMemoryManager
	 */
	public function removeRule(string $ruleId) : RbacMemoryManager
	{
		unset($this->_rules[$ruleId]);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\ProviderInterface::getRule()
	 */
	public function getRule(string $ruleId) : ?RuleInterface
	{
		if(!isset($this->_rules[$ruleId]))
		{
			return null;
		}
		
		$data = $this->_rules[$ruleId];
		
		return new Rule($data['id'], $data['name'], $data['data']);
	}
	
	/**
	 * Adds or replace the given group status.
	 * 
	 * @param string $statusId
	 * @param boolean $active
	 * @return RbacMemoryManager
	 */
	public function addGroupStatus(string $statusId, bool $active) : RbacMemoryManager
	{
		$this->_groupStatuses[$statusId] = ['id' => $statusId, 'active' => $active];
		
		return $this;
	}
	
	/**
	 * Removes the given group status.
	 * 
	 * @param string $statusId
	 * @return RbacMemoryManager
	 */
	public function removeGroupStatus(string $statusId) : RbacMemoryManager
	{
		unset($this->_groupStatuses[$statusId]);
		
		return $this;
	}
	
	/**
	 * Gets the group status that matches the given status id.
	 * 
	 * @param string $statusId
	 * @return GroupStatusInterface
	 */
	public function getGroupStatus(string $statusId) : GroupStatusInterface
	{
		if(!isset($this->_groupStatuses[$statusId]))
		{
			return new GroupStatus('default', false);
		}
		
		$data = $this->_groupStatuses[$statusId];
		
		return new GroupStatus($data['id'], $data['active']);
	}
	
	/**
	 * Adds or replace the given role status.
	 * 
	 * @param string $statusId
	 * @param boolean $active
	 * @return RbacMemoryManager
	 */
	public function addRoleStatus(string $statusId, bool $active) : RbacMemoryManager
	{
		$this->_roleStatuses[$statusId] = ['id' => $statusId, 'active' => $active];
		
		return $this;
	}
	
	/**
	 * Removes the given role status.
	 * 
	 * @param string $statusId
	 * @return RbacMemoryManager
	 */
	public function removeRoleStatus(string $statusId) : RbacMemoryManager
	{
		unset($this->_roleStatuses[$statusId]);
		
		return $this;
	}
	
	/**
	 * Gets the role status that matches the given status id.
	 * 
	 * @param string $statusId
	 * @return RoleStatusInterface
	 */
	public function getRoleStatus(string $statusId) : RoleStatusInterface
	{
		if(!isset($this->_roleStatuses[$statusId]))
		{
			return new RoleStatus('default', false);
		}
		
		$data = $this->_roleStatuses[$statusId];
		
		return new RoleStatus($data['id'], $data['active']);
	}
	
	/**
	 * Adds or replace the given user status.
	 * 
	 * @param string $statusId
	 * @param boolean $active
	 * @return RbacMemoryManager
	 */
	public function addUserStatus(string $statusId, bool $active) : RbacMemoryManager
	{
		$this->_userStatuses[$statusId] = ['id' => $statusId, 'active' => $active];
		
		return $this;
	}
	
	/**
	 * Removes the given user status.
	 * 
	 * @param string $statusId
	 * @return RbacMemoryManager
	 */
	public function removeUserStatus(string $statusId) : RbacMemoryManager
	{
		unset($this->_userStatuses[$statusId]);
		
		return $this;
	}
	
	/**
	 * Gets the user status that matches the given status id.
	 *
	 * @param string $statusId
	 * @return UserStatusInterface
	 */
	public function getUserStatus(string $statusId) : UserStatusInterface
	{
		if(!isset($this->_userStatuses[$statusId]))
		{
			return new UserStatus('default', false);
		}
		
		$data = $this->_userStatuses[$statusId];
		
		return new UserStatus($data['id'], $data['active']);
	}
	
}
