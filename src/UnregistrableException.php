<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use RuntimeException;

/**
 * UnregistrableException class file.
 * 
 * This is a simple implementation of the UnregistrableThrowable interface.
 * 
 * @author Anastaszor
 */
class UnregistrableException extends RuntimeException implements UnregistrableThrowable
{
	
	// nothing to add
	
}
