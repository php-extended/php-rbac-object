<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

/**
 * Rule class file.
 * 
 * This class is a simple implementation of the RuleInterface.
 * 
 * @author Anastaszor
 */
class Rule implements RuleInterface
{
	
	/**
	 * The identifier of the rule.
	 * 
	 * @var string
	 */
	protected string $_identifier;
	
	/**
	 * The name of the rule.
	 * 
	 * @var string
	 */
	protected string $_rulename;
	
	/**
	 * The dictionnary of values that must match.
	 * 
	 * @var array<string, string>
	 */
	protected array $_data;
	
	/**
	 * Builds a new Rule with its depdancies.
	 * 
	 * @param string $identifier
	 * @param string $rulename
	 * @param array<string, string> $data
	 */
	public function __construct(string $identifier, string $rulename, array $data = [])
	{
		$this->_identifier = $identifier;
		$this->_rulename = $rulename;
		$this->_data = $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_identifier.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RuleInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RuleInterface::getRulename()
	 */
	public function getRulename() : string
	{
		return $this->_rulename;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RuleInterface::getData()
	 */
	public function getData() : array
	{
		return $this->_data;
	}
	
	 /**
	  * {@inheritDoc}
	  * @see \PhpExtended\Rbac\RuleInterface::validate()
	  */
	 public function validate(UserInterface $user, RoleInterface $role, array $params = []) : bool
	 {
		foreach($this->_data as $key => $value)
		{
			if('user' === $key)
			{
				if($user->getIdentifier() !== $value)
				{
					return false;
				}
				
				continue;
			}
			
			if('role' === $key)
			{
				if($role->getIdentifier() !== $value)
				{
					return false;
				}
				
				continue;
			}
			
			if(!isset($params[$key]))
			{
				return false;
			}
			
			if($params[$key] !== $value)
			{
				return false;
			}
		}
		
		return true;
	 }
	
}
