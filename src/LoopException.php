<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

use InvalidArgumentException;

/**
 * LoopException class file.
 * 
 * This is a simple implementation of the LoopThrowable interface.
 * 
 * @author Anastaszor
 */
class LoopException extends InvalidArgumentException implements LoopThrowable
{
	
	// nothing to add
	
}
