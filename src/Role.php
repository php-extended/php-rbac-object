<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-rbac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Rbac;

/**
 * Role class file.
 * 
 * This is a simple implementation of the RoleInterface.
 * 
 * @author Anastaszor
 */
class Role implements RoleInterface
{
	
	/**
	 * The provider for the dependancies.
	 *
	 * @var ProviderInterface
	 */
	protected ProviderInterface $_provider;
	
	/**
	 * The identifier of the role.
	 *
	 * @var string
	 */
	protected string $_identifier;
	
	/**
	 * The name of the role.
	 *
	 * @var string
	 */
	protected string $_rolename;
	
	/**
	 * The status of the role.
	 *
	 * @var RoleStatusInterface
	 */
	protected RoleStatusInterface $_roleStatus;
	
	/**
	 * Builds a new Role with its dependancies.
	 * 
	 * @param ProviderInterface $provider
	 * @param string $identifier
	 * @param string $rolename
	 * @param RoleStatusInterface $roleStatus
	 */
	public function __construct(
		ProviderInterface $provider,
		string $identifier,
		string $rolename,
		RoleStatusInterface $roleStatus
	) {
		$this->_provider = $provider;
		$this->_identifier = $identifier;
		$this->_rolename = $rolename;
		$this->_roleStatus = $roleStatus;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@['.$this->_identifier.']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getIdentifier()
	 */
	public function getIdentifier() : string
	{
		return $this->_identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getRolename()
	 */
	public function getRolename() : string
	{
		return $this->_rolename;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getRoleStatus()
	 */
	public function getRoleStatus() : RoleStatusInterface
	{
		return $this->_roleStatus;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getRules()
	 */
	public function getRules() : array
	{
		try
		{
			return $this->_provider->getRulesFromRole($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a role object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getAssignedUsers()
	 */
	public function getAssignedUsers() : array
	{
		try
		{
			return $this->_provider->getUsersFromRole($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a role object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getAssignedGroups()
	 */
	public function getAssignedGroups() : array
	{
		try
		{
			return $this->_provider->getGroupsFromRole($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a role object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getParentRoles()
	 */
	public function getParentRoles() : array
	{
		try
		{
			return $this->_provider->getParentRoles($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a role object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getParentRolesRecursive()
	 */
	public function getParentRolesRecursive() : array
	{
		try
		{
			return $this->_provider->getParentRolesRecursive($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a role object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getChildrenRoles()
	 */
	public function getChildrenRoles() : array
	{
		try
		{
			return $this->_provider->getChildrenRoles($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a role object
			return [];
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Rbac\RoleInterface::getChildrenRolesRecursive()
	 */
	public function getChildrenRolesRecursive() : array
	{
		try
		{
			return $this->_provider->getChildrenRolesRecursive($this);
		}
		catch(UnprovidableThrowable $exc)
		{
			// should not happen as we are in a role object
			return [];
		}
	}
	
}
